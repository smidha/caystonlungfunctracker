package com.arches.CaystonLungFuncTracker;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;
import java.util.TimeZone;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import org.apache.commons.io.FileUtils;
import org.supercsv.io.CsvListWriter;
import org.supercsv.io.ICsvListWriter;
import org.supercsv.prefs.CsvPreference;
import com.arches.pgp.PGPTool;
import com.arches.utilities.DateTime;

public class CaystonLungFuncTrackerMain 
{
	
	public static String hibernatePath;
	public static String lftErrorLogsLocation; 
	public static Logger caystonLFTLogger;
	public static String lftTXTPath;
	public static String lftPGPPath;
	public static String passphrase;
	public static String key;
	public static String lftCSVFileLocation;
	public static String encryptedLftCSVFileLocation;
	public static String configFileLocation;
	public static String zipCodeValidationURL;
	static FileHandler fileHandler;
	
	
	public static void initializeApp()
	{
		configFileLocation="config";
		ResourceBundle configBundle=ResourceBundle.getBundle("config");
		
		hibernatePath=configBundle.getString("hibernatePath").trim();
		lftErrorLogsLocation=configBundle.getString("lftErrorLogsLocation").trim();
		//lftTXTPath=configBundle.getString("lftTXTPath").trim();
		//lftPGPPath=configBundle.getString("lftPGPPath").trim();
		passphrase=configBundle.getString("PASSPHRASE").trim();
		//key=configBundle.getString("KEY").trim();
		lftCSVFileLocation=configBundle.getString("lftCSVFileLocation").trim();
		encryptedLftCSVFileLocation=configBundle.getString("encryptedLftCSVFileLocation").trim();
		zipCodeValidationURL=configBundle.getString("zipCodeValidationURL").trim();
		
		
		try {
	  		fileHandler=new FileHandler(lftErrorLogsLocation+"LFTErrorLogs_"+DateTime.getDateTime()+".txt",true);
	  		caystonLFTLogger=Logger.getLogger("caystonLFTLogger");
	  		caystonLFTLogger.addHandler(fileHandler);
	  		SimpleFormatter simpleFormatter=new SimpleFormatter();
	  		fileHandler.setFormatter(simpleFormatter);
	  	   caystonLFTLogger.log(Level.INFO,"Logging for UTC date:"+DateTime.getDateTime(),"Logging for UTC date:"+DateTime.getDateTime());
	  	}
	  	catch(Exception e)
	  	{
	  		System.out.println("Exception in configuring logger."+e);
	  		e.printStackTrace();
	  	}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
			initializeApp();
			GetUsers users=new GetUsers();
			List<String> lines=users.getUsers();
			long count=0;
			if(lines!=null)
			{
				count=lines.size();
			}
			//boolean success=prepareFile(lines);
			try{
			boolean success=writeCSV(lines);
			if(success==false)
			{
				caystonLFTLogger.log(Level.SEVERE,"Error in writing records to csv file. Method returned false","[Main]prepareFile returned false");
			}
			}
			catch(Exception e)
			{
				e.printStackTrace();
				caystonLFTLogger.log(Level.SEVERE, e.getMessage(),e);
			}
				finally
				{
					for(Handler h:caystonLFTLogger.getHandlers())
					{ if(h!=null)
					    h.close();   //must call h.close or a .LCK file will remain.
					}
				}
				
			
			System.out.println(" processing complete..");
			
			System.exit(0);
	}

	public static boolean prepareFile(List<String> lines)
	{
		boolean success=false;
		if(lines==null)
		{
			lines=new ArrayList<String>();
		}
		long count=lines.size();
		lines.add(0,Calendar.getInstance(TimeZone.getTimeZone("EST")).getTime().toString());
		
		File file=new File(lftTXTPath+"ARCHES_LFT_CAYSTON_BACKPACK_ELIGIBLE_"+DateTime.getDateTime()+"_"+count+".txt");
		//true for append mode
		try {
			FileUtils.writeLines(file,lines,true);
			success=true;
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			success=false;
		}
		finally
		{
			return success;
		}
    }
	public static boolean writeCSV(List<String> list) {

		boolean success=true;
	    //create a File class object and give the file the name employees.csv
		String inputFilePath=lftCSVFileLocation+"ARCHES_GILEAD_CAYSTON_CRM_ENROLLMENT_"+DateTime.getDateTime()+"_"+list.size()+".csv";
		String outputFilePath=encryptedLftCSVFileLocation+"ARCHES_GILEAD_CAYSTON_CRM_ENROLLMENT_"+DateTime.getDateTime()+"_"+list.size()+".csv.pgp";
	if(list==null)
	{
		list=new ArrayList<String>();
	}
		
		Iterator it=list.iterator();
		String record[]=null;
		String header[]={"Contact","Address1","Address2","City","ST","Zip","Country","Phone"};
				ICsvListWriter csvWriter = null;
        try {
            csvWriter = new CsvListWriter(new FileWriter(inputFilePath), CsvPreference.STANDARD_PREFERENCE);
            csvWriter.writeHeader(header);
            for (int i = 0; i < list.size(); i++)
            {
            	record=list.get(i).replace("\"","".trim()).split(",");
               csvWriter.write(record);
              
            }
        	ResourceBundle resource = ResourceBundle.getBundle(CaystonLungFuncTrackerMain.configFileLocation);
    		PGPTool pgpTool=new PGPTool();
    		try {
    			
    			pgpTool.testEncrypt(inputFilePath,outputFilePath,resource.getString("PASSPHRASE"),resource.getString("LFTKEY"));
    		} catch (Exception e) {
    			// TODO Auto-generated catch block
    			
    			e.printStackTrace();
    			   CaystonLungFuncTrackerMain.caystonLFTLogger.log(Level.SEVERE,e.getMessage(),e);
    			   success=false;
    			
    		}
        } 
        catch (IOException e) 
        {
            e.printStackTrace(); // TODO handle exception properly
            CaystonLungFuncTrackerMain.caystonLFTLogger.log(Level.SEVERE,e.getMessage(),e);
            success=false;
        }
        finally {
            try
            {
                csvWriter.close();
            }
            catch (IOException e) {
            	e.printStackTrace();
            	  CaystonLungFuncTrackerMain.caystonLFTLogger.log(Level.SEVERE,e.getMessage(),e);
            	success=false;
            }
            return success;
        }
        
        } //end writeCSV()


}

