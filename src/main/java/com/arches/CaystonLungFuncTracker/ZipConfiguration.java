package com.arches.CaystonLungFuncTracker;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

@Configuration
public class ZipConfiguration {

	@Bean
	public Jaxb2Marshaller marshaller() {
		Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
		marshaller.setContextPath("com.arches.model.soapgenerated");
		return marshaller;
	}

	@Bean
	public ZipClient zipClient(Jaxb2Marshaller marshaller) {
		ZipClient client = new ZipClient();
		client.setDefaultUri("http://www.webserviceX.NET/GetInfoByZIP");
		client.setMarshaller(marshaller);
		client.setUnmarshaller(marshaller);
		return client;
	}

}