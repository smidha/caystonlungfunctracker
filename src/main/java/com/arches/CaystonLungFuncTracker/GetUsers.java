package com.arches.CaystonLungFuncTracker;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Level;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Projection;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.type.Type;
import org.json.JSONObject;
import org.json.XML;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

public class GetUsers {

	public static List<String> getUsers()
	{
		Configuration cfg=null;
		SessionFactory sessionFactory=null;
		Session session=null;
		List<String> lines=new ArrayList<String>();
		
		try
		{
			
			System.out.println("getting users..");
			cfg=new Configuration();
			cfg.configure(CaystonLungFuncTrackerMain.hibernatePath);
			sessionFactory=cfg.buildSessionFactory();
			session=sessionFactory.openSession();
	
			//gete all users from LFT
			String hql1="select UD.uuid,LOWER(UD.firstName),LOWER(UD.lastName),LOWER(UD.address1),LOWER(UD.address2),LOWER(UD.city),UPPER(UD.state),UD.zip,UD.contactNumber, count(1) from UserDet UD, LungFuncTracker LFT where UD.uuid=LFT.user_uid and UD.uuid is not null and (UD.status is null or UD.status!=1) and (UD.zip is not null AND LEN(UD.zip)>4)  and (UD.address1 is not null AND UD.address1!='') and (UD.city is not null AND UD.city!='' AND UPPER(UD.city)!='CITY') and (UD.state is not null AND UD.state!='' AND UPPER(UD.state)!='STATE*') group by UD.uuid,LOWER(UD.firstName),LOWER(UD.lastName),LOWER(UD.address1),LOWER(UD.address2),LOWER(UD.city),UPPER(UD.state),UD.zip, UD.contactNumber having count(1)>=2";
			String hql_update="update UserDet set status=1 where uuid=:uuidParam";
			Query query1= session.createQuery(hql1);
			Query query2=session.createQuery(hql_update);
					
			List results=query1.list();
			
			String line="".trim();
			System.out.println("iterating list..");
			ZipClient zipClient=new ZipClient();
			for (Iterator it = results.iterator(); it.hasNext();)
			{ 
				Object[] row = (Object[]) it.next();
				
				query2.setParameter("uuidParam",row[0]);
				line="";
				for (int i = 2; i < row.length-1; i++)
				
				{
					//check zip for validity or else skip the record
					String zip=(String) row[7];
					boolean zipCodeValid=true;
					try
					{
					zipCodeValid=zipClient.getZipResponse(zip.trim());
					
					}
					
					catch(Exception e)
					{
						System.out.println("Exception in verification via soap web service for zip code:"+zip+". "+e);
						e.printStackTrace();
					}
					if(zipCodeValid==false)
					{
						//problem in executing get method.skip the record
						System.out.println("invalid zip code."+zip+"Continue with next record");
						continue;
					}
					
					// iterate from 0 to (len-2). Avoid adding count() at len-1 to file
					String property=(String)row[i];
					property=org.apache.commons.lang3.text.WordUtils.capitalize(property);
					
					if(i==2)
					{
						//append first and last name
					property=org.apache.commons.lang3.text.WordUtils.capitalize(((String)row[1])+" "+(String)row[2]);	
					}
					
					//add country
					if(i==row.length-3)
					{
						property=property+"\",\""+"USA";
					}
					
					if(i!=row.length-2)
					{//append comma to each entry except last entry
						line=line+"\""+property+"\",";
					}
					else
					{
						line=line+"\""+property+"\"";
					}
				}	
				lines.add(line.replace("null","").trim());
				query2.executeUpdate();
				session.beginTransaction().commit();
				}
			}
				
			
				catch(Exception e)
		{
			e.printStackTrace();
			CaystonLungFuncTrackerMain.caystonLFTLogger.log(Level.SEVERE,e.getMessage(),e);
		}
		finally
		{
			if(session!=null)
			{
				session.flush();
				session.clear();
				session.close();
				session=null;
			}

			return lines;

		}
		
		
	}
public static String sendGet(String zip)
{
	String response ="".trim();
	String url;
	HttpURLConnection con = null ;
	try{
	url=CaystonLungFuncTrackerMain.zipCodeValidationURL.trim()+zip.trim();
		if(url==null || url.equals("") || url.equals("0"))
		{
			//validation off
		
			CaystonLungFuncTrackerMain.caystonLFTLogger.info("No web service end point provided.Assuming service is off. No zip code checking");
			return "off";
		}
	
		else
		{
		
			if(!zip.matches("[0-9\\-]*"))
			{
				return "invalid";
			}
		
			URL obj = new URL(url);
			con= (HttpURLConnection) obj.openConnection();

			// optional default is GET
			con.setRequestMethod("GET");



			int responseCode = con.getResponseCode();
			System.out.println("\nSending 'GET' request to URL : " + url);
			System.out.println("Response Code : " + responseCode);

			BufferedReader in = new BufferedReader(
			new InputStreamReader(con.getInputStream()));
			String inputLine;
		

			while ((inputLine = in.readLine()) != null)
			{
				response=response+(inputLine);
			}
			in.close();
		}
	System.out.println("response..."+response);
		//check response
		DocumentBuilderFactory dbf =
            DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        InputSource is = new InputSource();
        is.setCharacterStream(new StringReader(response));

        Document doc = db.parse(is);
        NodeList nodes = doc.getElementsByTagName("Table");

        // iterate the employees
        for (int i = 0; i < nodes.getLength(); i++) {
           Element element = (Element) nodes.item(i);

           NodeList st = element.getElementsByTagName("STATE");
           Element line = (Element)st.item(0);
          String state=getCharacterDataFromElement(line);
          NodeList ct = element.getElementsByTagName("CITY");
          line = (Element) ct.item(0);
          String city= getCharacterDataFromElement(line);
        

          if(state!=null && state!=null && city.length()>0 && state.length()>0)
          {
        	  System.out.println("valid...");
        	  return "valid";
          }
	 else
	 {
		 System.out.println("invalid...");
		 return "invalid";
	 }
	}
	}
	catch(Exception e)
	{
		
		CaystonLungFuncTrackerMain.caystonLFTLogger.log(Level.SEVERE,e.getMessage(),e);
		System.out.println("error...");
		return "error";
	}
			
	//print result
	finally
	{
		
		if(con!=null)
		{
			con.disconnect();
		}
		return response;
	}

}

	
	 public static String getCharacterDataFromElement(Element e) {
		    Node child = e.getFirstChild();
		    if (child instanceof org.w3c.dom.CharacterData) {
		       org.w3c.dom.CharacterData cd = (org.w3c.dom.CharacterData) child;
		       return cd.getData();
		    }
		    return "?";
		  }	
}
