package com.arches.CaystonLungFuncTracker;

import java.awt.List;
import java.util.Iterator;

import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.SoapMessage;
import org.springframework.ws.soap.client.core.SoapActionCallback;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.arches.model.soapgenerated.GetInfoByZIP;
import com.arches.model.soapgenerated.GetInfoByZIPResponse;
import com.arches.model.soapgenerated.ObjectFactory;

public class ZipClient extends WebServiceGatewaySupport{

	public boolean getZipResponse(String zip)
	{
		boolean valid=false;

	
		GetInfoByZIP request=new GetInfoByZIP();
		request.setUSZip(zip);
		
		WebServiceTemplate wst=getWebServiceTemplate();
		Jaxb2Marshaller m=new ZipConfiguration().marshaller();
		wst.setMarshaller(m);
		wst.setUnmarshaller(m);
		ObjectFactory o=new ObjectFactory();
		GetInfoByZIPResponse resp=o.createGetInfoByZIPResponse();
		
		 resp=(GetInfoByZIPResponse)wst.marshalSendAndReceive("http://www.webservicex.net/uszip.asmx",request,new SoapActionCallback("http://www.webserviceX.NET/GetInfoByZIP"));
		
		
		java.util.List<Object> list=resp.getGetInfoByZIPResult().getContent();
		System.out.println(" resp received of size........:"+list.size());
		Iterator<Object> it=list.iterator();
		while(it.hasNext())
		{
			System.out.println("next:"+it.next());
			Element root = (Element)list.get(0);
			if(root==null || root.getChildNodes()==null || root.getChildNodes().item(0)==null)
			{
				System.out.println("XML root is null or child nodes null..continue");
				continue;
			}
		      NodeList nodes = root.getChildNodes().item(0).getChildNodes();
		      System.out.println("nodes.getLength() "+nodes.getLength());
		      for(int i=0;i<nodes.getLength();i++){
		          Node node = nodes.item(i);
		          System.out.println(node.getNodeName()+" "+node.getFirstChild().getTextContent());
		          
		          if(node.getNodeName().equals("CITY") && node.getFirstChild().getTextContent().length()>0)
		          {
		        	  System.out.println("[zip client]valid zip code..returning true");
		        	  valid=true;
		          }
		      }
		}

	  
		return valid;
		
	}
}

